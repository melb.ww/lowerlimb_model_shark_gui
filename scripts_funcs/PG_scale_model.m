ccc

import org.opensim.modeling.*;

my_model=Model('C:\Users\wuw4\Documents\GIT_local\lowerlimb_model_shark_GUI\data_lfs\subject_data\SHARK056\Barefeet 1\SHARK056.osim');
my_model.initSystem;
% myState =  my_model.initSystem();
% ----muscle details
my_musc_set=my_model.getMuscles();

nMusc = my_musc_set.getSize();
my_musc_names=cell(nMusc,1);
scale_force=2;
for n=1:nMusc
    my_musc_names(n)=my_musc_set.get(n-1).getName();
    musc_temp=my_musc_set.get(n-1);
    MaxF_old=musc_temp.getMaxIsometricForce;
    musc_temp.setMaxIsometricForce(MaxF_old*scale_force)
end
my_model.print(['temp10.osim']);