function [c,ceq]=nonlcon(a,F_act_alongtendon_temp,F_pas_alongtendon_temp,MA,T)


% a_muscle=a(1:nMusc)';
% a_res=a(nMusc+1:end)';
Fm=F_act_alongtendon_temp.*a'+F_pas_alongtendon_temp;
% Fm=F_act_alongtendon_temp.*a';

% Fm=F_act_alongtendon_temp.*a';
ceq=Fm*MA-T;

c=[];
end