if 0
    ccc
else
    clear all
end

% Subject.name='SHARK052';
% Subject.name='SHARK055';
% Subject.name='SHARK061';
% Subject.name='SHARK063';
% Subject.name='SHARK064';
% Subject.name='SHARK066';
% Subject.name='SHARK070';
Subject.name='SHARK095';

% Subject.mass=790/9.8;
Subject.mass=0;
Subject.height=0;
Subject.LR=0;


Condition='Barefeet 1';



if 1
    C_modeltool.f_S_repo_C3D(Subject,Condition);
    folders_res=C_modeltool.f_S_repo_res(Subject,Condition,1);% ~,~,flag_remove_empty
    
    if 0 % Plot folder_res
        for n=1:length(folders_res)
            Lb2_temp=C_modeltool(Subject,Condition,folders_res(n).name,0);
            
            Lb2_temp.f_fig_subplot_T_interest_ID(1);
%             Lb2_temp.f_fig_subplot_res_OpenSim_SO_Fm(1,1);% flag_plot_acti,flag_GRF_event
            % Lb2_temp.f_fig_subplot_res_OpenSim_SO_reserve_force(0,1);% flag_plot_acti,flag_GRF_event
            Lb2_temp.f_fig_subplot_res_Knee_child(1); %flag_GRF_event
        end
    end
end




if 0
    winopen('..\docs\30Barefoot_SHARK_for_Adam_fromBen.xlsx')
end


%%  Static trc and Scale'
% static_trial=C_modeltool(Subject,Condition,'Cal 03');
static_trial=C_modeltool(Subject,Condition,'Cal 01',1);
if 1
    static_trial.f_GUI_c3d()
end
%%
%=== Export trc
if 1
    static_trial.f_writeTRCGRF_OS(1);
else
    static_trial.f_writeTRC_Mokka(1);
    static_trial.f_rotate_shift_trc('x',90,[0 0 0]);
end
%=== Scale model

static_trial.f_set_flag_PluginGait(0); %0 barefoot marker; 1 Plugin Gait

static_trial.f_setup_scale();
%%
static_trial.f_setup_scale_strengh(0); %0: scaled by BW, or specifc scale factor
% static_trial.f_setup_scale_strengh(2); %0: scaled by BW, or specifc scale factor

%%  Load Move Trial Class

% Lb2=C_modeltool(Subject,Condition,'Trial08');Lb2.setup_forceplate_LR={{'r','1'},{'r','3'},{'l','2'}};
% Lb2=C_modeltool(Subject,Condition,'Trial28',1);
Lb2=C_modeltool(Subject,Condition,'Trial33',1);
Lb2.setup_forceplate_LR={{'l','2'}};

% Lb2.joints_interest={};

% Lb2.f_setup_scale_strengh(0.01); %0: scaled by BW, or specifc scale factor



%
if 1
    Lb2.f_GUI_c3d
end

%%  Motion trc and IK

%=== Export trc
Lb2.f_writeTRCGRF_OS(1);%0 no rotate; 1 flag_rot90

Lb2.f_trim_mot_by_GRF()
% Lb2.ff_Trim_Marker_down();


%=== Setup IK
Lb2.f_setup_IK(1);


if 0
    Lb2.f_GUI_mot;
    Lb2.f_GUI_c3d;
end

Lb2.f_notificatin();

%%
% Setup up extermal loading
if ~exist(Lb2.file_setup_ExtLoads,'file')
    % Lb2.f_GUI_c3d
    Lb2.f_setup_ExtLoads(0);%flag_winopen_extLoadxml
end

%  Setup ID, run and load
if 1
    Lb2.f_load_model();
    Lb2.f_load_res_mot();
    Lb2.f_setup_run_ID(6);
    Lb2.f_load_res_ID();
end
Lb2.f_load_res_ID_interest();

%  Plot moment of Interest
Lb2.f_fig_subplot_T_interest_ID(1); % flag_GRF_event
% Lb2.f_fig_subplot_T_interest_ID_norm(1); % flag_GRF_event
%


%  $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
% Setup reserve force
Lb2.f_setup_reserve_actuator();


% Setup SO
Lb2.f_setup_run_SO_GUI();

if 0 %SO activation not good
    Lb2.f_fig_subplot_res_OpenSim_SO_Fm(1,1);% flag_plot_acti,flag_GRF_event
    static_trial=C_modeltool(Subject,Condition,'Cal 01',0);
    static_trial.f_setup_scale_strengh(0); %0: scaled by BW, or specifc scale factor
    % re-run SO
end
% GRF $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
Lb2.f_setup_run_GRF_GUI(0);% Expressed in: 0 Child, 1 Ground;
%
if 1
    Lb2.f_setup_run_GRF_GUI(1);% Expressed in: 0 Child, 1 Ground;
    Lb2.f_makefile_Knee_force_GUI();
%     Lb2.f_GUI_mot;
end

Lb2.f_notificatin();
%========================= Analyze END HERE=========================

%%
Lb2.f_fig_subplot_res_OpenSim_SO_Fm(1,1);% flag_plot_acti,flag_GRF_event
% Lb2.f_fig_subplot_res_OpenSim_SO_reserve_force(0,1);% flag_plot_acti,flag_GRF_event
Lb2.f_fig_subplot_res_Knee_child(1); %flag_GRF_event

%%   Script solver
if 0
%     MA for SO
    if 1
        %     Lb2.indx_act_muscle=[1:92]; % ALL
        Lb2.f_get_MA_Fvel();
    else
        Lb2.f_load_MA_Fvel();
    end
    
    %%  SO
    if 1
        
        if 1
            Lb2.f_load_res_ID_interest();
            Lb2.f_load_MA_Fvel();
        end
        Lb2.f_setup_run_SO(1);
        Lb2.f_save_res_SO();
    else
        Lb2.f_load_res_SO();
    end
    
    
    
    %%  Plot SO
    
    
    % Lb2.f_fig_subplot_res_a(Lb2.res_SO,1);% flag_GRF_event=1;
    Lb2.f_fig_subplot_res_Fm(Lb2.res_SO,1);% flag_GRF_event=1;
    Lb2.f_fig_subplot_res_a(Lb2.res_SO,1);
    
    %%  Knee force prepare
    
    % get LR ID
    if 1
        Lb2.f_setup_run_ID_LM(6,1);
        Lb2.f_setup_run_ID_LM(6,-1);
    end
    % get LR MA
    if 1
        Lb2.step_length=1;
        Lb2.f_get_MA_LM();
    end
    %%  Knee force and plot
    if 1
        if 1
            Lb2.f_load_res_ID_LM();
            Lb2.f_load_res_SO();
            Lb2.f_load_MA_LM();
        end
        
        Lb2.f_setup_run_KneeLoad();
        Lb2.f_save_res_Knee();
    else
        Lb2.f_load_res_Knee();
    end
    %
    flag_GRF_event=1;
    Lb2.f_fig_subplot_Knee(flag_GRF_event);
    
    %%  =================================================
end



%%  get analog
Lb2.f_extract_Analog();

%%  Copy c3d from Nexus to Repo
if 1
    folder_nexus='Z:\My Documents\Vicon Nexus Databases\SHARK Trial\SHARK Trial_PROCESSED\SHARK\';
    C_modeltool.f_S_copyNexusC3D_2_Repo(folder_nexus,' ',Subject,Condition);
end
%%
C_modeltool.f_S_repo_C3D(Subject,Condition);
if 0
    C_modeltool.f_S_data_summary;
end