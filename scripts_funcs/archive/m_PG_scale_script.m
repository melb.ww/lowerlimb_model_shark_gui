clear all
close all
import org.opensim.modeling.*

SCALEtool=ScaleTool('C:\OpenSim_4\model_1\Models\Gait2392_Simbody_Tim_Sayer\my_scale_TimSayer_v2.xml');

SCALEtool.getModelScaler.setMarkerFileName('C:\Users\wuw4\OneDrive - The University of Melbourne\Documents\Unimelb_research\Timothy_sayer_data\Tim cleaned\Dev24\Barefoot\my_processed\Dev24_cali.trc');
SCALEtool.getMarkerPlacer.setMarkerFileName('C:\Users\wuw4\OneDrive - The University of Melbourne\Documents\Unimelb_research\Timothy_sayer_data\Tim cleaned\Dev24\Barefoot\my_processed\Dev24_cali.trc');


SCALEtool.getModelScaler.setOutputModelFileName('C:\OpenSim_4\model_1\Models\Gait2392_Simbody_Tim_Sayer\model_api.osim');
SCALEtool.getMarkerPlacer.setOutputModelFileName('C:\OpenSim_4\model_1\Models\Gait2392_Simbody_Tim_Sayer\model_api.osim');

SCALEtool.print('C:\OpenSim_4\model_1\Models\Gait2392_Simbody_Tim_Sayer\my_scale_TimSayer_v2_temp.xml')

