%%  CCC and reset
clear all
close all
ccc
Subject.name='FOLK001';
Subject.mass=75.5;
Subject.height=1.7;
Condition='Barefeet';%''

%%  Static trc and Scale
static_trial=C_modeltool(Subject,Condition,'Cal 02');
if  ~(exist(static_trial.file_osim_scaled, 'file'))
    if ~(exist(static_trial.file_trc_rot, 'file'))
        %=== Export trc
        if 0
            static_trial.f_writeTRC_Mokka(1)
            input('==> Press Enter after trc has been exported...')
            %         static_trial.f_rotate_trc('x',90)
            static_trial.f_rotate_shift_trc('x',90,[20 30 44])
            static_trial.f_writeGRF_OS
        else
            static_trial.f_writeTRCGRF_OS
        end


    end
    %=== Scale model
    static_trial.f_setup_scale;
    static_trial.f_setup_osim_LM(1);
    static_trial.f_setup_osim_LM(-1);
else
    disp(' ')
    disp(['Model subject.osim already scaled, skip STATIC ... ',static_trial.file_osim_scaled])
end

%%  Load Move Trial Class

Lb2=C_modeltool(Subject,Condition,'Trial01');Lb2.setup_forceplate_LR={{'r','2'}};

% Lb2=C_modeltool(Subject,Condition,'walking02');Lb2.setup_forceplate_LR={{'r','1'},{'r','3'}};
% Lb2=C_modeltool(Subject,Condition,'DVJ_07');
% Lb2=C_modeltool(Subject,Condition,'DVJ_06');

Lb2.joints_interest={...
    'hip_flexion_r';...
    'hip_adduction_r';...
    'hip_rotation_r';...
    'knee_angle_r';...
    'ankle_angle_r';...
    'subtalar_angle_r';...
    'mtp_angle_r';...
    %
%     'hip_flexion_l';...
%     'hip_adduction_l';...
%     'hip_rotation_l';...
%     'knee_angle_l';...
%     'ankle_angle_l';...
%     'subtalar_angle_l';... 
%     'mtp_angle_l'...
    };

%%  Motion trc and IK
if ~(exist(Lb2.file_mot, 'file'))
    %=== Export trc
    if  ~exist(Lb2.file_GRF, 'file')
        if 1
            Lb2.f_writeTRCGRF_OS();
        else
            Lb2.f_writeTRC_Mokka(1);
            input('==> Press Enter after trc has been exported...');
            Lb2.f_rotate_shift_trc('x',90,[20 30 44]);
            Lb2.f_writeGRF_OS;
        end
        1
    else
        disp(' ')
        disp('ROT TRC already exported, skip moving TRC export ... ')
    end
    %=== Setup IK
    Lb2.f_setup_IK(1);
    input('==> Press Enter after IK ... ')
else
    disp(' ')
    disp(['Mot already scaled, skip IK  ... ',Lb2.file_mot])
end

%%  Setup up extermal loading
if 0
    Lb2.f_GUI_mot;
    Lb2.f_GUI_c3d;
end


if ~exist(Lb2.file_setup_ExtLoads,'file')
    % Lb2.f_GUI_c3d

    Lb2.f_setup_ExtLoads();
end

%%  Setup ID, run and load
if 1
    Lb2.f_load_model();
    Lb2.f_load_res_mot();
    Lb2.f_setup_run_ID(6);
    Lb2.f_load_res_ID();
end
Lb2.f_load_res_ID_interest();

%%  RRA
if 0
    Lb2.f_setup_RRA([0,100]);
    Lb2.f_run_RRA();
    Lb2.f_load_res_RRA;
end
if 0
%     Lb2.joints_interest=Free_joints;
    Lb2.f_load_res_RRA_interest();
    Lb2.f_fig_subplot_T_interest_RRA()
end

%%  Plot moment of Interest
Lb2.f_fig_subplot_T_interest_ID(1); % flag_GRF_event
% Lb2.f_fig_subplot_T_interest_ID_norm(1); % flag_GRF_event

%%  MA for SO

if 1
    Lb2.step_length=1;
    indx_act_muscle=[1:92];
    Lb2.f_get_MA_Fvel(indx_act_muscle);
else
    Lb2.f_load_MA_Fvel();
end

%%  SO
if 1
    Lb2.f_setup_run_SO(1);
    Lb2.f_save_res_SO();
else
    Lb2.f_load_res_SO();
end

%%  Plot SO


% Lb2.f_fig_subplot_res_a(Lb2.res_SO,1);% flag_GRF_event=1;
Lb2.f_fig_subplot_res_Fm(Lb2.res_SO,1);% flag_GRF_event=1;

%%  plot EMG, a

set_channelname={
    {28,'RecFem'},...
    {31,'VasLat'},...
    {29,'VasMed'},...
    {38,'TibAnt'},...
    {33,'LatGas'},...
    {32,'MedGas'},...
    {9,'LatHam'},...
    {7,'MedHam'},...
    {8,'MedHam'}...
%      {34,'MedGas'},...
    };

if 0
    set_MVC={'MVC','MVC01','MVC02','MVC03','MVC04_standingcalf','MVC05','MVC06'};
    Lb2.f_load_MVC_c3d(set_MVC,set_channelname);
else
    Lb2.f_load_MVC_mat;
end

Lb2.f_extract_proc_norm_EMG(set_channelname);

Lb2.f_fig_subplot_EMG(1);% flag_norm;

Lb2.f_fig_subplot_res_a_EMG([1:43],1,0);% [flag_GRF_event,flag_norm]

%%  Knee force and plot
if 0
    Lb2.f_setup_run_ID_LM(6,1);
    Lb2.f_setup_run_ID_LM(6,-1);
end
Lb2.f_load_res_ID_LM();

if 1
    Lb2.step_length=1;
    indx_ML_muscle=[7:33];
%     indx_ML_muscle=[7,8,9,10,11,17,19,32,33]
    Lb2.f_get_MA_LM(indx_ML_muscle);
else
    Lb2.f_load_MA_LM();
end


Lb2.f_setup_run_KneeLoad();
flag_GRF_event=1;
Lb2.f_fig_subplot_Knee(flag_GRF_event);

%%  =================================================

%%  get analog
Lb2.f_extract_Analog();

%%  Copy c3d from Nexus to Repo
if 1
folder_nexus='C:\Users\wuw4\OneDrive - The University of Melbourne\Documents\Unimelb_research\2019_FOLK\Folk trial\FOLK\';
C_modeltool.f_copyNexusC3D_2_Repo(folder_nexus,' ',Subject,Condition);
end
