clear all
import org.opensim.modeling.*

%% input C3D file
input_folder_path_root='C:\Users\wuw4\OneDrive - The University of Melbourne\Documents\Unimelb_research\Timothy_sayer_data\Tim cleaned\';
subject_='Dev24';

% condition_='Kayano';
condition_='Barefoot';


% filename_input=[subject_,' Cal 01'];
filename_input=[subject_,' Running 08'];

%% output file
out_folder_path_root='C:\OpenSim_4\model_1\Models\Gait2392_Simbody_Tim_Sayer\Subj_spec_model_data\';

folder_path_prod_output=fullfile(out_folder_path_root,subject_,condition_);
if exist(folder_path_prod_output, 'dir')~=7
    mkdir(folder_path_prod_output)
end

%%
filename_=strrep(filename_input, ' ', '_');
file_c3d=fullfile([fullfile(input_folder_path_root,subject_,condition_,filename_input),'.c3d']);
file_trc=fullfile(folder_path_prod_output,[filename_,'.trc']);
file_grf=fullfile(folder_path_prod_output,[filename_,'_grf.mot']);
file_emg=fullfile(folder_path_prod_output,[filename_,'_EMG.mat']);

%%  trc grf
if 1
    c3d = osimC3D_v2(file_c3d);
    c3d.writeTRC(file_trc);
%     c3d.writeMOT(file_grf);
end
