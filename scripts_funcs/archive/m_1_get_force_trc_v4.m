clear all

%% input C3D file
path_input_root='C:\Users\wuw4\OneDrive - The University of Melbourne\Documents\Unimelb_research\2017_Timothy_sayer_data\Tim cleaned\';


subject_='Dev36';
condition_='Barefoot';
subj_mass=35; % C:\Users\wuw4\OneDrive - The University of Melbourne\Documents\Unimelb_research\Timothy_sayer_data\Tim cleaned\DevBiomech_CombinedDatabase.xlsx
% condition_='Kayano';
% condition_='Zaraca';

file_input=[subject_,' Running 03 cut'];
% file_input=[subject_,' DVJ D 09 cut'];
% file_input=[subject_,' Running 09 cut'];
%% output file

path_out_root_0='C:\OpenSim_4\model_1\Models\Gait2392_Simbody_Tim_Sayer\';
path_out_root=fullfile(path_out_root_0,'Subj_spec_model_data\');
folder_path_prod_output=fullfile(path_out_root,subject_,condition_);
if exist(folder_path_prod_output, 'dir')~=7
    mkdir(folder_path_prod_output)
end

%%
filename_=strrep(file_input, ' ', '_');
file_c3d=fullfile([fullfile(path_input_root,subject_,condition_,file_input),'.c3d']);
file_trc=fullfile(folder_path_prod_output,[filename_,'.trc']);
file_trc_cali=fullfile(folder_path_prod_output,'cali.trc');
file_grf=fullfile(folder_path_prod_output,[filename_,'_grf.mot']);
file_emg=fullfile(folder_path_prod_output,[filename_,'_EMG.mat']);
file_IK_mot_out=fullfile(folder_path_prod_output,[filename_,'.mot']);
file_ID_setup_out=fullfile(folder_path_prod_output,[subject_,'_',condition_,'_scale_setup.xml']);
file_osim=fullfile(folder_path_prod_output,[subject_,'_',condition_,'_scaled.osim']);
file_scale_setup=[path_out_root_0,'my_scale_TimSayer_v2.xml'];
file_IK_setup_0=[path_out_root_0,'my_IK_v0.xml'];
file_IK_setup_SubjSpec=fullfile(folder_path_prod_output,[subject_,'_',condition_,'_IK_setup.xml']);

disp(file_c3d);
if 1
    %%  trc grf
    import org.opensim.modeling.*;disp(' ');disp(' ');
    
    if 0
        c3d = osimC3D(file_c3d,1);
        c3d.rotateData('x',-90);
        c3d.writeTRC(file_trc);
        c3d.writeMOT(file_grf);
        f_replaceinfile('\-nan\(ind\)', '0', file_grf);
        f_replaceinfile('nan', '0', file_grf);
    end
    
    %%  EMG
    if 0
        % ------------Read-------------
        %     itf = c3dserver();
        itf = actxserver('C3DServer.C3D');
        openc3d(itf, 0, file_c3d);
        data_analog_set = f_get_c3d_AnalogSet(itf);
        p_load_variables_EMG
        ratio=itf.GetAnalogVideoRatio;
        n_frame=itf.GetVideoFrameRate;
        % ---------Preparation------------
        
        EMG=[];
        figure
        for n=1:length(set_channelname)-2
            channelname=char(set_channelname(n));
            
            ind_temp=find(ismember(data_analog_set.set_channel_name,channelname));
            
            
            subplot(8,1,n)
            plot(data_analog_set.time_EMG_adj,abs(data_analog_set.set_ACHANNEL_out(:,ind_temp)))
            title(data_analog_set.set_channel_name(ind_temp))
        end
        save(file_emg,'data_analog_set')
    end
    
    %%
    disp('------------------------------')
    disp('========= Open with Mokka, and pick up the range, export to TRC  =========')
    disp(fullfile([fullfile(path_input_root,subject_,condition_,[subject_,' Cal 01']),'.c3d']));
    disp(file_trc_cali);
    
    %%
    % ================= Setup Inverse Dynamics setup files
    
    SCALEtool=ScaleTool(file_scale_setup);
    SCALEtool.setSubjectMass(subj_mass);
    SCALEtool.getModelScaler.setMarkerFileName(file_trc_cali);
    SCALEtool.getMarkerPlacer.setMarkerFileName(file_trc_cali);
    
    SCALEtool.getModelScaler.setOutputModelFileName(file_osim);
    SCALEtool.getMarkerPlacer.setOutputModelFileName(file_osim);
    
    SCALEtool.print(file_ID_setup_out);
    
    disp('------------------------------------OPENSIM scale-------------------------------')
    disp('========= Use generic OpenSim model =========')
    disp([path_out_root_0,'gait2392_simbody_TimSayer_0.osim'])
    disp('========= Name of the Scale setup file =========')
    disp(file_ID_setup_out)
    
    disp('------------------------------------OPENSIM IK-------------------------------')
    disp('========= Load scaled osim model =========')
    disp(file_osim)
    
    IK_tool=InverseKinematicsTool(file_IK_setup_0);
    
    IK_tool.setMarkerDataFileName(file_trc);
    
    IK_tool.setOutputMotionFileName(file_IK_mot_out);
    
    IK_tool.print(file_IK_setup_SubjSpec);
    
    disp('========= Name of the IK setup file')
    disp(file_IK_setup_SubjSpec)
    disp('========= IK output')
    disp(file_IK_mot_out)
    
    %%
end