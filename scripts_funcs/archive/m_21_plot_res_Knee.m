if 1
    ccc
else
    clear all
end
Subject.name='SHARK097';
% Subject.name='SHARK099';
% Subject.name='SHARK100';
% Subject.name='SHARK102';
% Subject.name='SHARK104';

Subject.mass=0;
Subject.height=0;
Subject.LR=0;

SetCondition={'Barefeet 1','Shoes 1','Barefeet 2','Old Study Shoes 2'};
% SetCondition={'Barefeet 2','Old Study Shoes 2'};
% SetCondition={'Barefeet 1','Barefeet 2'};
% SetCondition={'Barefeet 1','Shoes 1'};
SetColor={'blue','green','red','black'};
% SetCondition={'Barefeet 1','Shoes 1'};
% SetCondition={'Shoes 1','Barefeet 2','Old Study Shoes 2'};
% figure

for n_condition=1:length(SetCondition)
    Condition_temp=char(SetCondition(n_condition));
    folders_res=C_modeltool.f_S_repo_res(Subject,Condition_temp);
    temp_color=char(SetColor(n_condition));
    %%
%     figure
    for n=1:length(folders_res)
        temp_trial=folders_res(n).name;
        
        temp_file=[folders_res(n).folder,'\',folders_res(n).name,'\res_Knee.mat'];
        Ini_end_cut_length=10;
        
        Lb2=C_modeltool(Subject,Condition_temp,temp_trial);
        Lb2.f_joints_interest_append('knee_abd');
        
        if 0
            
            Lb2.f_load_res_ID_interest();
            
%             Lb2.f_fig_subplot_T_interest_ID(1); % flag_GRF_event
            Lb2.f_fig_subplot_T_interest_ID_stack(1,temp_color); % flag_GRF_event
        end
        
        if 0
            Lb2.f_load_res_Knee();
            Lb2.f_fig_subplot_Knee(1);
        end
        
        if 0
            Lb2.f_load_res_SO();
            % Lb2.f_fig_subplot_res_a(Lb2.res_SO,1);% flag_GRF_event=1;
             Lb2.f_fig_subplot_res_Fm(Lb2.res_SO,1);% flag_GRF_event=1;
            Lb2.f_fig_subplot_res_a(Lb2.res_SO,1);
        end
        
        if 1
            load(temp_file)
            
            indx=Ini_end_cut_length:length(res_Knee.F_L_all)-Ini_end_cut_length;
            max_F(n)=max(res_Knee.F_all(indx));
            max_F_L(n)=max(res_Knee.F_L_all(indx));
            max_F_M(n)=max(res_Knee.F_M_all(indx));
            
            
            set_max(1,n)=max(res_Knee.F_all(indx));
            set_max(2,n)=max(res_Knee.F_L_all(indx));
            set_max(3,n)=max(res_Knee.F_M_all(indx));
            
            
            
            if n==length(folders_res)
%                 figure 
                subplot(2,4,n_condition)
                bar(set_max,'DisplayName','set_max')
                ylim([0 3500])
                grid on;
                title(Condition_temp)
            end
            if n==length(folders_res)
                set_max_normed=set_max./repmat(max_F,3,1);
%                 figure 
                subplot(2,4,n_condition+4)
                bar(set_max_normed,'DisplayName','set_max')
                ylim([0 1.2])
                grid on;
                title(Condition_temp)
            end     
            
        end
    end
    clear set_max set_max_normed
end

%%
% Lb2=C_modeltool(Subject,Condition,'Trial20');
%
% Lb2.f_load_res_Knee();
%
% flag_GRF_event=0;
% Lb2.f_fig_subplot_Knee(flag_GRF_event);