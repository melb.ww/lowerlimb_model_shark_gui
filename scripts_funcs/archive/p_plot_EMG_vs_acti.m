figure 
set_EMG_plot={
    {28,'RecFem'},...
    {31,'VasLat'},...
    {29,'VasMed'},...
    {38,'TibAnt'},...
    {33,'LatGas'},...
    {32,'MedGas'},...
    {9,'LatHam'},...
    {10,'LatHam'},...
    {7,'MedHam'},...
    {8,'MedHam'}...
    };
load(file_emg);
%     data_analog_set.time_EMG_adj

indx_time=data_analog_set.time_EMG_adj>=time_pick(1)&data_analog_set.time_EMG_adj<=time_pick(2);
    figure
for n=1:length(set_EMG_plot)
    
    
    
    
    ind_OS_muscle_temp=set_EMG_plot{n}{1};
    EMG_temp=set_EMG_plot{n}{2};
    ind_EMG_temp=find(ismember(data_analog_set.set_channel_name,EMG_temp));
    
    subplot (3,4,n)
    
    plot(data_analog_set.time_EMG_adj(indx_time)+0.00,abs(data_analog_set.set_ACHANNEL_out(indx_time,ind_EMG_temp)));
    
    hold on
    
    
%     subplot(3,4,n)
    plot (time_count,a(:,ind_OS_muscle_temp),'LineWidth',2.5);
    
    
%     plot(data_analog_set.time_EMG_adj(indx_time),abs(data_analog_set.set_ACHANNEL_out(indx_time,ind_EMG_temp)));
    
    
    
    
end