if 1
    ccc
else
    clear all
end


% Subject.name='SHARK104';
Subject.name='SHARK091';

Subject.mass=960/9.8;
Subject.height=0;
Subject.LR=0;
% Subject.LR='L'; %L, R, LR



Condition='Barefeet 1';
Lb2=C_modeltool(Subject,Condition,'Trial08');Lb2.setup_forceplate_LR={{'r','1'}};




%%  SO - GUI
% if 1
%     
%     if 1
%         Lb2.f_load_res_ID_interest();
%         Lb2.f_load_MA_Fvel();
%     end
%     Lb2.f_setup_run_SO(1);
%     Lb2.f_save_res_SO();
% else
%     Lb2.f_load_res_SO();
% end



% ccc
import org.opensim.modeling.*;
% a=AnalyzeTool('setup_SO_1.xml');
% a.run();
time_start=1.2;
time_end=2;


AT=AnalyzeTool('temp_in.xml');
% AT.setForceSetFiles('SO_Actuator.XML');
% AT=AnalyzeTool();
AT.setModelFilename(Lb2.file_osim_scaled);
AT.setResultsDir(Lb2.folder_res_SO_GUI);
AT.setInitialTime(time_start)
AT.setFinalTime(time_end)
AT.setSolveForEquilibrium(0)    
AT.setCoordinatesFileName(Lb2.file_mot)
AT.setExternalLoadsFileName(Lb2.file_setup_ExtLoads)
AT.setLowpassCutoffFrequency(6)



AT.getAnalysisSet.get(0).setStartTime(time_start);
AT.getAnalysisSet.get(0).setEndTime(time_end);
AT.print('temp2.xml')
AT.run()
