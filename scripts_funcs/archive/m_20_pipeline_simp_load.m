%% Load 
clear all
close all
ccc
Subject.name='Scott';
Subject.mass=60;
Subject.height=1.7;
Condition='Polit_03042019';%'Barefoot'

%% Load model

% Lb2=C_modeltool(Subject,Condition,'running06');Lb2.setup_forceplate_LR={{'r','3'}};
Lb2=C_modeltool(Subject,Condition,'walking02');Lb2.setup_forceplate_LR={{'r','1'},{'r','3'}};
% Lb2=C_modeltool(Subject,Condition,'DVJ_07');
% Lb2=C_modeltool(Subject,Condition,'DVJ_06');

%%
Free_joints={...
    'hip_flexion_r';...
    'hip_adduction_r';...
    'hip_rotation_r';...
    'knee_angle_r';...
    'ankle_angle_r';...
    'subtalar_angle_r';...
    'mtp_angle_r';...
    };
Lb2.joints_interest=Free_joints;
Lb2.f_load_res_ID_interest();


%%
if 0
    Lb2.step_length=1;
    indx_act_muscle=[1:92];
    Lb2.f_get_MA_Fvel(indx_act_muscle);
else
    Lb2.f_load_MA_Fvel();
end
%%
if 0
    Lb2.f_setup_run_SO(1);
    Lb2.f_save_res_SO();
else
    Lb2.f_load_res_SO();
end
% Lb2.f_l

%%
% Lb2.f_extract_Analog();
%%

set_channelname={
    {28,'RecFem'},...
    {31,'VasLat'},...
    {29,'VasMed'},...
    {38,'TibAnt'},...
    {33,'LatGas'},...
    {32,'MedGas'},...
    {9,'LatHam'},...
    {7,'MedHam'},...
    {8,'MedHam'}...
%     {34,'MedGas'},...
    };

if 0
    set_MVC={'MVC','MVC01','MVC02','MVC03','MVC04_standingcalf','MVC05','MVC06'};
    Lb2.f_load_MVC_c3d(set_MVC,set_channelname);
else
    Lb2.f_load_MVC_mat;
end

Lb2.f_extract_proc_norm_EMG(set_channelname);

Lb2.f_fig_subplot_EMG(1);% flag_norm;


Lb2.f_fig_subplot_res_a_EMG([1:43],1,0);% [flag_GRF_event,flag_norm]

%% Run ID - MA for LM models 
if 0
    Lb2.f_setup_run_ID_LM(6,1);
    Lb2.f_setup_run_ID_LM(6,-1);
end
Lb2.f_load_res_ID_LM();

if 0
    Lb2.step_length=1;
    indx_ML_muscle=[7:33];
    Lb2.f_get_MA_LM(indx_ML_muscle);
else
    Lb2.f_load_MA_LM();
end


Lb2.f_setup_run_KneeLoad();
flag_GRF_event=1;
Lb2.f_fig_subplot_Knee(flag_GRF_event);
