clear all
% close all
Subject='Dev12';
Condition='Kayano';%'Barefoot'


%%   Static trc and Scale
static_trial=C_modeltool(Subject,Condition,'Cal 02',49.5);
if  ~(exist(static_trial.file_osim_scaled, 'file'))
    if ~(exist(static_trial.file_trc_rot, 'file'))
        %=== Export trc
        static_trial.f_writeTRC_Mokka(1)
        input('==> Press Enter after trc has been exported...')
        static_trial.f_rotate_trc('x',90)
    end
    %=== Scale model
    static_trial.f_setup_scale;
else
    disp(' ')
    disp(['Model subject.osim already scaled, skip STATIC ... ',static_trial.file_osim_scaled])
end

%% motion trc and IK

Lb2=C_modeltool(Subject,Condition,'DVJ ND 03',999);
if ~(exist(Lb2.file_mot, 'file'))
    %=== Export trc
    if  ~exist(Lb2.file_GRF, 'file')
        Lb2.f_writeTRCGRF_OS()
    else
        disp(' ')
        disp('ROT TRC already exported, skip moving TRC export ... ')
    end
    %=== Setup IK
    Lb2.f_setup_IK(1);
    input('==> Press Enter after IK ... ')
else
    disp(' ')
    disp(['Mot already scaled, skip IK  ... ',Lb2.file_mot])
end
%%   Setup up extermal loading
if 0
    Lb2.f_GUI_mot;
    Lb2.f_GUI_c3d;
end

if ~exist(Lb2.file_setup_ExtLoads,'file')
    setup_forceplate_LR={...
        {'l','2'},...
        %     {'r','1'},...
        %     {'l','3'},...
        };
    Lb2.f_setup_ExtLoads(setup_forceplate_LR);
end
%%  Setup ID and run
Lb2.f_load_model();
Lb2.f_load_res_mot();
% Lb2.f_setup_run_ID(6);
Lb2.f_load_res_ID();

%%  RRA

Lb2.f_setup_RRA([0,100]);
Lb2.f_run_RRA();
Lb2.f_load_res_RRA;



%%  View Joint Torques
% Lb2.f_set_T_interest(Lb2.OS.name_coor);
% Lb2.f_fig_subplot_T_interest();
% Lb2.f_fig_plot_T_interest();
% Lb2.f_fig_waterfall_T_interest;

%%
Free_joints={...
    %     'hip_flexion_r';...
    %     'hip_adduction_r';...
    %     'hip_rotation_r';...
    %     'knee_angle_r';...
    %     'ankle_angle_r';...
    %     'subtalar_angle_r';...
    %     'mtp_angle_r'...
    %
    'hip_flexion_l';...
    'hip_adduction_l';...
    'hip_rotation_l';...
    'knee_angle_l';...
    'ankle_angle_l';...
    'subtalar_angle_l';...
    'mtp_angle_l'...
    };

Lb2.f_set_T_interest(Free_joints);
Lb2.f_fig_subplot_T_interest();

%%
if 0
    Lb2.step_length=1;
    indx_act_muscle=[1:92];
    Lb2.f_get_MA_Fvel(indx_act_muscle);
else
    Lb2.f_load_MA_Fvel();
end


