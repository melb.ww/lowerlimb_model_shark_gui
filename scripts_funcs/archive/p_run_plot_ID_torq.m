import org.opensim.modeling.*

File_model=file_osim;
% File_model='C:\OpenSim_4\model_1\Models\Gait2392_Simbody_Tim_Sayer\Subj_spec_model_data\Dev36\Barefoot\Dev36_Barefoot_scaled_rightlightweight.osim'
File_motion=file_IK_mot_out;
%   File_motion='C:\OpenSim_4\model_1\Models\Gait2392_Simbody_Tim_Sayer\Subj_spec_model_data\Dev36\Barefoot\Dev36_Running_03_cut_smooth.mot';
File_GRF=file_grf;

my_model=Model(File_model);

setup_forceplate_LR={...
    {'r','1'},...
     {'l','3'},...
    };

extLoadsObject = ExternalLoads();
extLoadsObject.setName('ExLoad_API');
extLoadsObject.setDataFileName(File_GRF);
extLoadsObject.setLowpassCutoffFrequencyForLoadKinematics(6);
for ind=1:length(setup_forceplate_LR)
    LR=setup_forceplate_LR{ind}{1};
    Num_plate=setup_forceplate_LR{ind}{2};
    
    temp_ext_force=ExternalForce;
    temp_ext_force.setName(['ex_force_',LR]);
    temp_ext_force.set_applied_to_body(['calcn_',LR]);
    temp_ext_force.set_force_expressed_in_body('ground');
    temp_ext_force.set_point_expressed_in_body('ground');
    temp_ext_force.set_force_identifier(['ground_force_',Num_plate,'_v']);
    temp_ext_force.set_point_identifier(['ground_force_',Num_plate,'_p']);
    temp_ext_force.set_torque_identifier(['ground_moment_',Num_plate,'_m']);
    extLoadsObject.set(ind-1,temp_ext_force);
    
end
extLoadsObject.print([pwd, '\support_files\temp_my_ext_force.xml']);
% ============ ID ============

IDsetupFile=[pwd,'\support_files\setup_id_api_no_model_no_external.xml'];
IDtool=InverseDynamicsTool(IDsetupFile);
IDtool.setResultsDir([pwd,'\support_files\ID_temp']);
IDtool.setModel(my_model)
IDtool=f_Set_ID_mot(IDtool,File_motion );

 IDtool.setExternalLoadsFileName([pwd, '\support_files\temp_my_ext_force.xml']);

IDtool.run();

%%
Free_joints_={...
    'hip_flexion_r';...
    'hip_adduction_r';...
    'hip_rotation_r';...
    'knee_angle_r';...
    'ankle_angle_r';...
    'subtalar_angle_r';...
    'mtp_angle_r'...
    %
        'hip_flexion_l';...
        'hip_adduction_l';...
        'hip_rotation_l';...
        'knee_angle_l';...
        'ankle_angle_l';...
        'subtalar_angle_l';...
        'mtp_angle_l'...
    };

Free_joints=Free_joints_;
nCoor_SO=length(Free_joints);
full_ID_file=[char(IDtool.getResultsDir()),'\',char(IDtool.getOutputGenForceFileName())];
my_ID_imp=importdata(full_ID_file);
my_ID_imp.colheaders(:,1) = [];
time_mot=my_ID_imp.data(:,1);
my_ID_imp.data(:,1) = [];

% get interested ID datae

T_interest=[];
for n=1:nCoor_SO
    %     temp_index=my_model.getCoordinateSet().getIndex(char(Free_joints(n)))+1;
    temp_index=    find(ismember(my_ID_imp.colheaders,[char(Free_joints(n)),'_moment']));
    T_interest=[T_interest,my_ID_imp.data(:,temp_index) ];
end

% clear IDtool;


%%
if 1
    figure
    for n=1:nCoor_SO
        subplot(3,5,n)
        
        plot (time_mot,T_interest(:,n),'LineWidth',2.5);
        title([num2str(n),'  ',char(Free_joints(n))],'fontsize',10,'Interpreter', 'none');
        xlim([time_mot(1),time_mot(end)])
        %     ylim([min(T_interest_TS(:))*1.2,max(T_interest_TS(:))*1.2])
        ylim([-200 100])
        %     ylim([-120,35])
        grid on;
    end
    
end