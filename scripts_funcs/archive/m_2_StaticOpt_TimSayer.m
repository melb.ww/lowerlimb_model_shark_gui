clear all
% close all
import org.opensim.modeling.*

global my_model my_musc_set my_musc_names my_coord_names...
    nMusc myState Lom Lst Fom Vmax
%%
% exp_session='TF10_EMG_Day1';
%
% % exp_trail='vb1bl';
% % exp_trail='vb1';
% % exp_trail='vb2';
% % exp_trail='vb3';
% exp_trail='vb4';
% % exp_trail='vb5';
% % exp_trail='vb6';
% % exp_trail='vb7';
% % exp_trail='vb8';

%%
% exp_session='TF10_EMG_Day3';
% % exp_trail='dyn2bl';
% exp_trial='dyn10';
% % exp_trail='dyn14';

%%

% 'NW0_2'

% exp_trial='NW0_1';
% exp_trial='NW0_2';%corrupted, bad data with ankle
% exp_trial='NW0_3';

% exp_trial='NW20_1';
% exp_trial='NW20_2';%corrupted, Huge ID data
% exp_trial='NW20_3';

% exp_trial='W0_2'; % unstable between cycles

exp_trial='W20_2'; % unstable between cycles
%%
flag_EMG=0;
flag_picktime=0;
flag_simple_force=1; % 1 ideal actuator, 0 Hill-type model
Scale_Fom=3;

step_length=1;

able_side='l';
Free_joints_={...
    'hip_flexion_r';...
    'hip_adduction_r';...
    'hip_rotation_r';...
    'knee_angle_r';...
    'ankle_angle_r';...
    'subtalar_angle_r';...
    'mtp_angle_r'...
    
    'hip_flexion_l';...
    'hip_adduction_l';...
    'hip_rotation_l';...
    'knee_angle_l';...
    'ankle_angle_l';...
    'subtalar_angle_l';...
    'mtp_angle_l'...
    };

% Free_joints = cellfun(@(x)[x,able_side],Free_joints_,'UniformOutput',false);
Free_joints=Free_joints_;
nCoor_SO=length(Free_joints);
%%
save_flag=1;
result_fold='\results\XXX\';
OS_folder='C:\OpenSim_33\Models\Gait2392_Simbody_OS33';

%%
% Path_trial=[pwd,'\Exp_data\',exp_session,'\',exp_trial];
% File_model=[pwd,'\Exp_data\',exp_session,'\gait2392_simbody_scaled.osim'];
% File_model=[pwd,'\Exp_data\',exp_session,'\gait2392_simbody_scaled_v4.osim'];

File_model=[OS_folder,'\gait2392_simbody_v2_33.osim'];

File_model='C:\OpenSim_4\model_1\Models\Gait2392_Simbody_Tim_Sayer\gait2392_simbody_PG1_scaled_v2_nomarker.osim';
File_motion=[OS_folder,'\data_v33\IK_result_v2_33.mot'];
File_GRF=[OS_folder,'\data_v33\test_data_forces.mot'];
% File_motion=[pwd,'\Exp_data\',exp_session,'\',exp_trial,'\Visual3d_SIMM_input.mot'];
% File_GRF=[pwd,'\Exp_data\',exp_session,'\',exp_trial,'\Visual3d_SIMM_grf.mot'];
% File_EMG=[pwd,'\Exp_data\',exp_session,'\EMG_V3D.mat'];
% File_total_length=[pwd,'\Exp_data\',exp_session,'\',exp_trial,'\file_length.txt'];
%
options = optimset('Algorithm','interior-point','Display','off');
%%   Load model
disp('Loading model')

my_model=Model(File_model);
myState =  my_model.initSystem();

% ----muscle details
my_musc_set=my_model.getMuscles();
nMusc = my_musc_set.getSize();
my_musc_names=cell(nMusc,1);
for n=1:nMusc
    my_musc_names(n)=my_musc_set.get(n-1).getName();
    Lom(n)=my_musc_set.get(n-1).getOptimalFiberLength();
    Fom(n)=my_musc_set.get(n-1).getMaxIsometricForce();
    Lst(n)=my_musc_set.get(n-1).getTendonSlackLength();
    Vmax(n)=Lom(n)*my_musc_set.get(n-1).getMaxContractionVelocity();
end
if exist('disable_muscle_ind','var')
    Fom(disable_muscle_ind)=0;
end
Fom=Fom*Scale_Fom;

indx_actvmuscle=[44:86];
indx_actvmuscle=[1:92];
nMusc_pick=length(indx_actvmuscle);
musc_active=my_musc_names(indx_actvmuscle);
% ----coord details
my_coor_set=my_model.getCoordinateSet();
nCoord_all = my_coor_set.getSize();
my_coord_names=cell(nCoord_all,1);
for n=1:nCoord_all
    my_coord_names(n)=my_coor_set.get(n-1).getName();
end


% if (~(exist ([Path_trial,'\time_pick.csv'], 'file')==2)) || flag_picktime
%     h=figure;
%     for i=1:nCoor_SO
%         subplot(2,4,i)
%         plot(time_mot,[T_interest(:,i),T_interest_ft(:,i)])
%         title(string(Free_joints(i)),'Interpreter', 'none')
%         ylim([min(T_interest(:))*1.05,max(T_interest(:))*1.05])
%     end
%     [time_pick,~] = ginput(2);
%     close(h)
%     csvwrite([pwd,'\Exp_data\',exp_session,'\',exp_trial,'\time_pick.csv'],time_pick)
% else
%     time_pick=csvread([Path_trial,'\time_pick.csv']);
% end

%%  Obtain kinematics data-----------

time_pick=[1.6, 2.5];
disp('Obtaining kinematics data...')

[my_mot_imp,delimiterOut,headerlinesOut] = importdata(File_motion);
my_mot_imp.colheaders=my_mot_imp.colheaders(2:24);
time_all=my_mot_imp.data(:,1);
indx_time=time_all>=time_pick(1)&time_all<=time_pick(2);
my_mot_imp.data = my_mot_imp.data (indx_time,:);

time=my_mot_imp.data(:,1);

my_mot_imp.data(:,1) = [];

my_mot_imp.data=my_mot_imp.data/180*pi;
[nFrame_mot, ~] = size(my_mot_imp.data);
nCoord_mot=length(my_mot_imp.colheaders);




%% Pre-run the Loop to get muscle moment arm, velocity, etc.--------------
disp('Pre-running the Loop to get muscle moment arm, velocity, etc...')

TS_count=0;
a_0=zeros(nMusc_pick,1);lb=a_0;
a_1=ones(nMusc_pick,1);ub=a_1;

for TimeStep=1:step_length:nFrame_mot
    TS_count=TS_count+1;
    
    time_count(TS_count)=time(TimeStep);
    mot_count(TS_count,:)=my_mot_imp.data(TimeStep,:);
    
    % =======================update the motion file to the myState(TS)
    
%     for index_coor=1:nCoord_mot
%         myValue=my_mot_imp.data(TimeStep,index_coor);
%         my_model.setStateVariable(myState,my_mot_imp.colheaders(index_coor), myValue);
%     end
    
    for index_coor=1:nCoord_mot
        myValue=my_mot_imp.data(TimeStep,index_coor);
        %         my_model.setStateVariableValue(myState,my_mot_imp.colheaders(index_coor), myValue);
%         my_mot_imp.colheaders(index_coor)
        my_coor_set.get(my_mot_imp.colheaders(index_coor)).setValue(myState, myValue)
    end
    
    %     for index_coor=1:nCoord_mot
    %         myValue=my_mot_imp.data(TimeStep,index_coor);
    %         my_model.setStateVariableValue(myState,my_mot_imp.colheaders(index_coor), myValue);
    %     end
    
    %     for index_coor=1:nCoord_mot
    %         myValue=my_mot_imp.data(TimeStep,index_coor);
    %         my_model.setStateVariableValue(myState,my_mot_imp.colheaders(index_coor), myValue);
    %     end
    
    %  ======================= Muscles analysis ;
    my_model.equilibrateMuscles(myState);
    for index_mus=1:nMusc_pick
        my_Lmt_temp(index_mus)=my_musc_set.get(char(musc_active(index_mus))).getLength(myState);
        %   ------------------------- Moment arm
        for index_coor=1:1:nCoor_SO
            coor_temp=my_coor_set.get(char(Free_joints(index_coor)));
            MA_temp= my_musc_set.get(char(musc_active(index_mus))).computeMomentArm(myState,coor_temp);
            my_MA_temp(index_mus,index_coor)=MA_temp;... % my_MA_temp=(muscle * coord)
                my_MA(TS_count, index_mus,index_coor)= MA_temp; % my_MA=(timframe * muscle * coord)
        end
    end
    my_Lmt(TS_count,:)=my_Lmt_temp;
end
%%
% obtain normalized fibre velocity
d_my_Lmt=[my_Lmt;zeros(1,nMusc_pick)]-[zeros(1,nMusc_pick);my_Lmt];
my_Lmt_dot=d_my_Lmt(2:TS_count,:)/(time_count(2)-time_count(1));
my_Lmt_dot=[my_Lmt_dot(1,:);my_Lmt_dot];
for n=1:TS_count
    my_Lmt_dot_norm(n,:)=my_Lmt_dot(n,:)./Vmax(indx_actvmuscle);
end

%%  obtain ID data-----------
disp('Running inverse dynamics (ID)...')

IDsetupFile=[pwd,'\support_files\setup_id_api_no_model_no_external.xml'];
IDtool=InverseDynamicsTool(IDsetupFile);
IDtool.setResultsDir([pwd,'\support_files\ID_temp']);
IDtool.setModel(my_model)
IDtool=f_Set_ID_mot(IDtool,File_motion );

% extLoadsObject = ExternalLoads(my_model,[OS_folder, '\data_v33\my_ex_force_v2_33.xml']);
extLoadsObject = ExternalLoads([OS_folder, '\data_v33\my_ex_force_v2_33.xml'],1);
% extLoadsObject=ExternalLoads([pwd, '\support_files\template_my_v3d_ext_force.xml'],1);
extLoadsObject.setDataFileName(File_GRF);
extLoadsObject.print([pwd, '\support_files\temp_my_ext_force.xml']);
IDtool.setExternalLoadsFileName([pwd, '\support_files\temp_my_ext_force.xml']);

IDtool.run();

full_ID_file=[char(IDtool.getResultsDir()),'\',char(IDtool.getOutputGenForceFileName())];
my_ID_imp=importdata(full_ID_file);
my_ID_imp.colheaders(:,1) = [];
time_mot=my_ID_imp.data(:,1);
my_ID_imp.data(:,1) = [];

% get interested ID datae

T_interest=[];
for n=1:nCoor_SO
%     temp_index=my_model.getCoordinateSet().getIndex(char(Free_joints(n)))+1;
    temp_index=    find(ismember(my_ID_imp.colheaders,[char(Free_joints(n)),'_moment']));
    T_interest=[T_interest,my_ID_imp.data(:,temp_index) ];
end

% Filter moment from ID...
disp('Filtering moment from ID...')
order_L = 2;
freq_L = 3;
[b_l,a_l] = butter(order_L, freq_L/(100/2), 'low');
% T_interest_ft = filtfilt(b_l, a_l, T_interest);

T_interest_ft = T_interest;

T_interest_ft=T_interest_ft(indx_time,:);
	
%%
TS_count=0;
tic
for TimeStep=1:step_length:nFrame_mot
    TS_count=TS_count+1;
    disp(['Timestep ' num2str(TS_count) ' under optimization...']);
    my_Lmt_temp=my_Lmt(TS_count,:);
    my_MA_temp=squeeze(my_MA(TS_count, :,:));
    
    if length(Free_joints)==1
        my_MA_temp=my_MA_temp';
    end
    
    my_Lmt_dot_temp=my_Lmt_dot_norm(TS_count,:);
    %++++++++++++++++++++++++++++++++++++++++++++++++++++
    [F_act_alongtendon_temp,F_pas_alongtendon_temp]=f_getforces(a_1,my_Lmt_temp,my_Lmt_dot_temp,indx_actvmuscle);
    
    if flag_simple_force
        F_pas_alongtendon_temp=zeros(1,nMusc_pick);
        for nnn=1:nMusc_pick
            F_act_alongtendon_temp(nnn)=my_musc_set.get(char(musc_active(nnn))).getMaxIsometricForce()*Scale_Fom;
        end
    end
    
    F_act_alongtendon(TS_count,:)=F_act_alongtendon_temp;
    F_pas_alongtendon(TS_count,:)=F_pas_alongtendon_temp;
    T_interest_TS(TS_count,:)=T_interest_ft(TimeStep,:);
    
    
    [a(TS_count,:),fval(TS_count),exitflag]=fmincon(@f_SO_obj,a_0,[],[],[],[],lb,ub,@(a)f_nonlcon(a,F_act_alongtendon_temp,F_pas_alongtendon_temp,my_MA_temp,T_interest_ft(TimeStep,:)),options);
    Fm(TS_count,:)=F_act_alongtendon_temp.*a(TS_count,:)+F_pas_alongtendon_temp;
end
%%
figure
for n=1:nMusc_pick
    subplot(10,10,n)
    plot (time_count,Fm(:,n),'LineWidth',2.5);
    title([num2str(n),'  ',char(musc_active(n))],'fontsize',10,'Interpreter', 'none');
    xlim([time_count(1),time_count(end)])
    %     ylim([-max(Fm(:))*.05,max(Fm(:))*1.35])
    ylim([-10,max(Fm(:)*1.05)])
    %     ylim([-10,1000])
    grid on;
    hold on
end

%%
figure
for n=1:nMusc_pick
    subplot(10,10,n)
    plot (time_count,a(:,n),'LineWidth',2.5);
    title([num2str(n),'  ',char(musc_active(n))],'fontsize',10,'Interpreter', 'none');
    xlim([time_count(1),time_count(end)])
    %     ylim([-max(Fm(:))*.05,max(Fm(:))*1.35])
    ylim([0,1])
    %     ylim([-10,1000])
    grid on;
    hold on
end

%%
if flag_EMG
    load(File_EMG);
    total_length=importdata(File_total_length)*10;
    
    [nrows,~] = cellfun(@size,VM_emg);
    Nmb_trial=find(nrows==total_length);
    frm_pick=(round(time_pick*100)+1)*10;
    % EMG_set={'BF_emg','GASL_emg','GMED_emg','GMEDp_emg','RF_emg','TA_emg','VL_emg','VM_emg'};
    %     EMG_set={'BF_emg','GASL_emg','GMED_emg','GMED_emg','GMED_emg','TA_emg','VL_emg','VM_emg'};
    %     indx_in_osim=[9,  33,        1,          2,         3,         38,       31,     29    ];
    
    EMG_set={'GMED_emg','GMED_emg','GMED_emg','BF_emg','VM_emg','VL_emg','GASL_emg','TA_emg'};
    indx_in_osim=[1,          2,         3,      9,       29,        31,      33,       38 ];
    
    time_emg=linspace(time_pick(1),time_pick(2),-frm_pick(1)+frm_pick(2)+1);
    % BF - biceps femoris (long head or short head?)
    % GASL - gastrocnemius lateral head
    % GMED - gluteus medius
    % GMED_P - gluteus medius (posterior)
    % RF - Not measured?
    % TA - tib anterior
    % VL - vastus lateralis
    % VM - vastus medialis
    
    for n=1:length(EMG_set)
        expression=['temp_data=',char(EMG_set(n)),';'];
        eval(expression);
        temp_data=cell2mat(temp_data(Nmb_trial));
        temp_data_picked=abs(detrend(temp_data(frm_pick(1):frm_pick(2))));
        subplot(5,9,indx_in_osim(n))
        plot(time_emg,temp_data_picked*3000+750)
        
        %     ylim([-0.01 0.3])
        %     title([num2str(n),'  ',char(EMG_set(n))],'fontsize',10,'Interpreter', 'none');
    end
    
    figure
    for n=1:length(EMG_set)
        subplot(3,3,n)
        plot (time_count,Fm(:,indx_in_osim(n)),'LineWidth',2.5);
        
        hold on
        
        expression=['temp_data=',char(EMG_set(n)),';'];
        eval(expression);
        temp_data=cell2mat(temp_data(Nmb_trial));
        temp_data_picked=abs(detrend(temp_data(frm_pick(1):frm_pick(2))));
        plot(time_emg,temp_data_picked*3000+750)
        
        title([num2str(indx_in_osim(n)),'  ',char(musc_active(indx_in_osim(n)))],'fontsize',10,'Interpreter', 'none');
        xlim([time_count(1),time_count(end)])
        %     ylim([-max(Fm(:))*.05,max(Fm(:))*1.35])
        ylim([-10,1500])
        grid on;
        hold on
    end
end
%%


if 0
    figure
    for n=1:nCoor_SO-1
        subplot(3,2,n)
        
        plot (time_count,T_interest_TS(:,n),'LineWidth',2.5);
        title([num2str(n),'  ',char(Free_joints(n))],'fontsize',10,'Interpreter', 'none');
        xlim([time_count(1),time_count(end)])
        %     ylim([min(T_interest_TS(:))*1.2,max(T_interest_TS(:))*1.2])
        ylim([-119 18])
        %     ylim([-120,35])
        grid on;
    end
    
end