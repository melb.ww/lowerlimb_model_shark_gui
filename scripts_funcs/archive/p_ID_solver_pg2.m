clear all
import org.opensim.modeling.*

File_model='C:\OpenSim_4\model_1\Models\Gait2392_Simbody_Tim_Sayer\gait2392_simbody_TimSayer_0.osim';
% File_motion='C:\OpenSim_4\model_1\Models\Gait2392_Simbody_Tim_Sayer\Subj_spec_model_data\Dev36\Barefoot\Dev36_Running_03_cut.mot';
File_motion='C:\OpenSim_4\model_1\Models\Gait2392_Simbody_Tim_Sayer\Subj_spec_model_data\Dev36\Barefoot\Dev36_Running_03_cut_rad_flat.mot';

my_model=Model(File_model);

my_sto=Storage(File_motion);
my_analset=my_model.getAnalysisSet();

% my_model.getSimbodyEngine.convertDegreesToRadians(my_sto);

my_GCV=GCVSplineSet(5,my_sto);


% myState =  my_model.initSystem();

% setup_forceplate_LR={...
%     {'r','1'},...
%     {'l','3'},...
%     };
% extLoadsObject = ExternalLoads();
% % extLoadsObject.setName('ExLoad_API');
% extLoadsObject.setDataFileName('C:\OpenSim_4\model_1\Models\Gait2392_Simbody_Tim_Sayer\Subj_spec_model_data\Dev36\Barefoot\Dev36_Running_03_cut_grf.mot');
% extLoadsObject.setLowpassCutoffFrequencyForLoadKinematics(6);
% for ind=1:length(setup_forceplate_LR)
%     LR=setup_forceplate_LR{ind}{1};
%     Num_plate=setup_forceplate_LR{ind}{2};
%     
%     temp_ext_force=ExternalForce;
%     temp_ext_force.setName(['ex_force_',LR]);
%     temp_ext_force.set_applied_to_body(['calcn_',LR]);
%     temp_ext_force.set_force_expressed_in_body('ground');
%     temp_ext_force.set_point_expressed_in_body('ground');
%     temp_ext_force.set_force_identifier(['ground_force_',Num_plate,'_v']);
%     temp_ext_force.set_point_identifier(['ground_force_',Num_plate,'_p']);
%     temp_ext_force.set_torque_identifier(['ground_moment_',Num_plate,'_m']);
%     extLoadsObject.set(ind-1,temp_ext_force);
%     
% end
% my_model.addComponent(extLoadsObject)


 myState =  my_model.initSystem();

idSolver = InverseDynamicsSolver(my_model);

     my_model.equilibrateMuscles(myState);
%     my_model.realizeAcceleration(myState);

my_analset.step(myState,2)
f_vec2array(idSolver.solve(myState,my_GCV,0))