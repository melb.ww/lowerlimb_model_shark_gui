% RUN after m_1_get_force_trc.m
% close all
import org.opensim.modeling.*

global my_model my_musc_set my_musc_names my_coord_names...
    nMusc myState Lom Lst Fom Vmax

%%
setup_forceplate_LR={...
    {'r','1'},...
    {'l','3'},...
    };


flag_EMG=0;
flag_picktime=0;
flag_simple_force=1; % 1 ideal actuator, 0 Hill-type model
Scale_Fom=1;

step_length=1;

able_side='l';
Free_joints_={...
    'hip_flexion_r';...
%     'hip_adduction_r';...
%     'hip_rotation_r';...
%     'knee_angle_r';...
%     'ankle_angle_r';...
%     'subtalar_angle_r';...
    'mtp_angle_r'...
    %
    %         'hip_flexion_l';...
    %         'hip_adduction_l';...
    %         'hip_rotation_l';...
    %         'knee_angle_l';...
    %         'ankle_angle_l';...
    %         'subtalar_angle_l';...
    %         'mtp_angle_l'...
    };

Free_joints=Free_joints_;
nCoor_SO=length(Free_joints);
%%
save_flag=1;
result_fold='\results\XXX\';

%%

File_model=file_osim;
% File_model='C:\OpenSim_4\model_1\Models\Gait2392_Simbody_Tim_Sayer\Subj_spec_model_data\Dev36\Barefoot\Dev36_Barefoot_scaled_rightlightweight.osim'

File_motion=file_IK_mot_out;
File_GRF=file_grf;


options = optimset('Algorithm','interior-point','Display','off');
%%   Load model
disp('Loading model')

my_model=Model(File_model);
myState =  my_model.initSystem();

% ----muscle details
my_musc_set=my_model.getMuscles();
nMusc = my_musc_set.getSize();
my_musc_names=cell(nMusc,1);
for n=1:nMusc
    my_musc_names(n)=my_musc_set.get(n-1).getName();
    Lom(n)=my_musc_set.get(n-1).getOptimalFiberLength();
    Fom(n)=my_musc_set.get(n-1).getMaxIsometricForce();
    Lst(n)=my_musc_set.get(n-1).getTendonSlackLength();
    Vmax(n)=Lom(n)*my_musc_set.get(n-1).getMaxContractionVelocity();
end
if exist('disable_muscle_ind','var')
    Fom(disable_muscle_ind)=0;
end
Fom=Fom*Scale_Fom;

indx_actvmuscle=[44:86];
indx_actvmuscle=[1:92];
nMusc_pick=length(indx_actvmuscle);
musc_active=my_musc_names(indx_actvmuscle);
% ----coord details
my_coor_set=my_model.getCoordinateSet();
nCoord_all = my_coor_set.getSize();
my_coord_names=cell(nCoord_all,1);
for n=1:nCoord_all
    my_coord_names(n)=my_coor_set.get(n-1).getName();
end


%%  Obtain kinematics data-----------

time_pick=[1.6, 2.5];
time_pick=[0, 1];
disp('Obtaining kinematics data...')
[my_mot_imp,nFrame_mot,nCoord_mot] = f_mot_read(File_motion,time_pick);

%% Pre-run the Loop to get muscle moment arm, velocity, etc.--------------
disp('Pre-running the Loop to get muscle moment arm, velocity, etc...')

TS_count=0;
a_0=zeros(nMusc_pick,1);lb=a_0;
a_1=ones(nMusc_pick,1);ub=a_1;

for TimeStep=1:step_length:nFrame_mot
    TS_count=TS_count+1;
    
    time_count(TS_count)=my_mot_imp.time(TimeStep);
    mot_count(TS_count,:)=my_mot_imp.data(TimeStep,:);
    
    % =======================update the motion file to the myState(TS)
    for index_coor=1:nCoord_mot
        myValue=my_mot_imp.data(TimeStep,index_coor);
        %         my_model.setStateVariableValue(myState,my_mot_imp.colheaders(index_coor), myValue);
        %         my_mot_imp.colheaders(index_coor)
        my_coor_set.get(my_mot_imp.colheaders(index_coor)).setValue(myState, myValue)
    end
    
    %  ======================= Muscles analysis ;
    my_model.equilibrateMuscles(myState);
    for index_mus=1:nMusc_pick
        my_Lmt_temp(index_mus)=my_musc_set.get(char(musc_active(index_mus))).getLength(myState);
        %   ------------------------- Moment arm
        for index_coor=1:1:nCoor_SO
            coor_temp=my_coor_set.get(char(Free_joints(index_coor)));
            MA_temp= my_musc_set.get(char(musc_active(index_mus))).computeMomentArm(myState,coor_temp);
            my_MA_temp(index_mus,index_coor)=MA_temp;... % my_MA_temp=(muscle * coord)
                my_MA(TS_count, index_mus,index_coor)= MA_temp; % my_MA=(timframe * muscle * coord)
        end
    end
    my_Lmt(TS_count,:)=my_Lmt_temp;
end
%%
% obtain normalized fibre velocity
my_Lmt_dot=diff(my_Lmt)/(time_count(2)-time_count(1));
my_Lmt_dot=[my_Lmt_dot(1,:);my_Lmt_dot];
my_Lmt_dot_norm=my_Lmt_dot./repmat(Vmax,size(my_Lmt_dot,1),1);

%%
disp('Running inverse dynamics (ID)...')

extLoadsObject = ExternalLoads();
extLoadsObject.setName('ExLoad_API');
extLoadsObject.setDataFileName(File_GRF);
extLoadsObject.setLowpassCutoffFrequencyForLoadKinematics(6);
for ind=1:length(setup_forceplate_LR)
    LR=setup_forceplate_LR{ind}{1};
    Num_plate=setup_forceplate_LR{ind}{2};
    
    temp_ext_force=ExternalForce;
    temp_ext_force.setName(['ex_force_',LR]);
    temp_ext_force.set_applied_to_body(['calcn_',LR]);
    temp_ext_force.set_force_expressed_in_body('ground');
    temp_ext_force.set_point_expressed_in_body('ground');
    temp_ext_force.set_force_identifier(['ground_force_',Num_plate,'_v']);
    temp_ext_force.set_point_identifier(['ground_force_',Num_plate,'_p']);
    temp_ext_force.set_torque_identifier(['ground_moment_',Num_plate,'_m']);
    extLoadsObject.set(ind-1,temp_ext_force);
    
end
extLoadsObject.print([pwd, '\support_files\temp_my_ext_force.xml']);
% ============ ID ============

IDsetupFile=[pwd,'\support_files\setup_id_api_no_model_no_external.xml'];
IDtool=InverseDynamicsTool(IDsetupFile);
IDtool.setResultsDir([pwd,'\support_files\ID_temp']);
IDtool.setModel(my_model)
IDtool=f_Set_ID_mot(IDtool,File_motion );

IDtool.setExternalLoadsFileName([pwd, '\support_files\temp_my_ext_force.xml']);

IDtool.run();

full_ID_file=[char(IDtool.getResultsDir()),'\',char(IDtool.getOutputGenForceFileName())];
my_ID_imp=importdata(full_ID_file);
my_ID_imp.colheaders(:,1) = [];
time_mot=my_ID_imp.data(:,1);
my_ID_imp.data(:,1) = [];

% get interested ID datae

T_interest=[];
for n=1:nCoor_SO
    %     temp_index=my_model.getCoordinateSet().getIndex(char(Free_joints(n)))+1;
    temp_index=    find(ismember(my_ID_imp.colheaders,[char(Free_joints(n)),'_moment']));
    T_interest=[T_interest,my_ID_imp.data(:,temp_index) ];
end

% Filter moment from ID...
disp('Filtering moment from ID...')
order_L = 2;
freq_L = 3;
[b_l,a_l] = butter(order_L, freq_L/(100/2), 'low');
% T_interest_ft = filtfilt(b_l, a_l, T_interest);

T_interest_ft = T_interest;

% T_interest_ft=T_interest_ft(indx_time,:);

%%
TS_count=0;
tic
for TimeStep=1:step_length:nFrame_mot
    TS_count=TS_count+1;
    disp(['Timestep ' num2str(TS_count) ' under optimization...']);
    my_Lmt_temp=my_Lmt(TS_count,:);
    my_MA_temp=squeeze(my_MA(TS_count, :,:));
    
    if length(Free_joints)==1
        my_MA_temp=my_MA_temp';
    end
    
    my_Lmt_dot_temp=my_Lmt_dot_norm(TS_count,:);
    %++++++++++++++++++++++++++++++++++++++++++++++++++++
    [F_act_alongtendon_temp,F_pas_alongtendon_temp]=f_getforces(a_1,my_Lmt_temp,my_Lmt_dot_temp,indx_actvmuscle);
    
    if flag_simple_force
        F_pas_alongtendon_temp=zeros(1,nMusc_pick);
        for nnn=1:nMusc_pick
            F_act_alongtendon_temp(nnn)=my_musc_set.get(char(musc_active(nnn))).getMaxIsometricForce()*Scale_Fom;
        end
    end
    
    F_act_alongtendon(TS_count,:)=F_act_alongtendon_temp;
    F_pas_alongtendon(TS_count,:)=F_pas_alongtendon_temp;
    T_interest_TS(TS_count,:)=T_interest_ft(TimeStep,:);
    
    
    [a(TS_count,:),fval(TS_count),exitflag]=fmincon(@f_SO_obj,a_0,[],[],[],[],lb,ub,@(a)f_nonlcon(a,F_act_alongtendon_temp,F_pas_alongtendon_temp,my_MA_temp,T_interest_ft(TimeStep,:)),options);
    Fm(TS_count,:)=F_act_alongtendon_temp.*a(TS_count,:)+F_pas_alongtendon_temp;
end
%%
if 1
    if 0
        path_SO_ouput=['.\results\',filename_,'.mat'];
        save(path_SO_ouput,'Fm');
    end
    
    figure
    for n=1:nMusc_pick
        subplot(10,10,n)
        plot (time_count,Fm(:,n),'LineWidth',2.5);
        title([num2str(n),'  ',char(musc_active(n))],'fontsize',10,'Interpreter', 'none');
        xlim([time_count(1),time_count(end)])
        %     ylim([-max(Fm(:))*.05,max(Fm(:))*1.35])
        ylim([-10,max(Fm(:)*1.05)])
        %     ylim([-10,1000])
        grid on;
        hold on
    end
    
    %%
    figure
    for n=1:nMusc_pick
        subplot(10,10,n)
        plot (time_count,a(:,n),'LineWidth',2.5);
        title([num2str(n),'  ',char(musc_active(n))],'fontsize',10,'Interpreter', 'none');
        xlim([time_count(1),time_count(end)])
        %     ylim([-max(Fm(:))*.05,max(Fm(:))*1.35])
        ylim([0,1])
        %     ylim([-10,1000])
        grid on;
        hold on
    end
    
    %%
    if 1
        set_EMG_plot={
            {28,'RecFem'},...
            {31,'VasLat'},...
            {29,'VasMed'},...
            {38,'TibAnt'},...
            {33,'LatGas'},...
            {32,'MedGas'},...
            {9,'LatHam'},...
            {10,'LatHam'},...
            {7,'MedHam'},...
            {8,'MedHam'}...
            };
        load(file_emg);
        %     data_analog_set.time_EMG_adj
        
        indx_time=data_analog_set.time_EMG_adj>=time_pick(1)&data_analog_set.time_EMG_adj<=time_pick(2);
        %     figure
        for n=1:length(set_EMG_plot)
            
            ind_OS_muscle_temp=set_EMG_plot{n}{1};
            EMG_temp=set_EMG_plot{n}{2};
            ind_EMG_temp=find(ismember(data_analog_set.set_channel_name,EMG_temp));
            
            subplot(10,10,ind_OS_muscle_temp)
            
            plot(data_analog_set.time_EMG_adj(indx_time),abs(data_analog_set.set_ACHANNEL_out(indx_time,ind_EMG_temp)));
        end
        
    end
    %%
    
    if 1
        figure
        for n=1:nCoor_SO-1
            subplot(3,2,n)
            
            plot (time_count,T_interest_TS(:,n),'LineWidth',2.5);
            title([num2str(n),'  ',char(Free_joints(n))],'fontsize',10,'Interpreter', 'none');
            xlim([time_count(1),time_count(end)])
            %     ylim([min(T_interest_TS(:))*1.2,max(T_interest_TS(:))*1.2])
            ylim([-200 100])
            %     ylim([-120,35])
            grid on;
        end
        
    end
end