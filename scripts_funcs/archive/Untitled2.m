ccc
import org.opensim.modeling.*;

% file_model='C:\OpenSim_4\model_1\Models\Gait2392_Simbody_Tim_Sayer\gait2392_simbody_TimSayer_0_JRF.osim';
file_model='C:\Users\wuw4\Documents\GIT_local\lowerlimb_model_myasics\data_lfs\subject_data\Scott\Scott.osim';

my_model=Model(file_model);
%%
JS=my_model.getJointSet;
KneeR=JS.get('knee_r');
% KneeR.append_coordinates(KneeR.get_coordinates(0));
%%
f0=KneeR.get_frames(0);
vec3_trans=f0.get_translation();
vec3_trans_new=vec3_trans;
lateral_trans=vec3_trans.get(1)/(-0.4)*0.025;
vec3_trans_new.set(2,lateral_trans);
f0.set_translation(vec3_trans_new);


f1=KneeR.get_frames(1);
vec3_trans=f1.get_translation();
vec3_trans_new=vec3_trans;
vec3_trans_new.set(2,lateral_trans);
f1.set_translation(vec3_trans_new);
% 
%%
my_model.print('temp2.osim');