clear all
% close all
Subject='Stella';
Condition='Polit_25032019';%'Barefoot'
%%
if 0  % copy c3d from Nexus to Repo
folder_nexus_base='C:\Users\wuw4\OneDrive - The University of Melbourne\Documents\Unimelb_research\2019_ARC-l_mytest\Pilot_test_25032019_copy1\Pilot_test_25032019\Pilot\Stella\Polit_25032019\Polit_25032019_';
folder_nexus=[folder_nexus_base,''];
C_modeltool.f_copyNexusC3D_2_Repo(folder_nexus,Subject,Condition);
end
%%   Static trc and Scale
static_trial=C_modeltool(Subject,Condition,'MVC',58);
if  ~(exist(static_trial.file_osim_scaled, 'file'))
    if ~(exist(static_trial.file_trc_rot, 'file'))
        %=== Export trc
        static_trial.f_writeTRC_Mokka(1)
        input('==> Press Enter after trc has been exported...')
%         static_trial.f_rotate_trc('x',90)
        static_trial.f_rotate_shift_trc('x',90,[20 30 44])
        static_trial.f_writeGRF_OS
        1
%         static_trial.f_writeTRCGRF_OS

    end
    %=== Scale model
    static_trial.f_setup_scale;
else
    disp(' ')
    disp(['Model subject.osim already scaled, skip STATIC ... ',static_trial.file_osim_scaled])
end

%% motion trc and IK

Lb2=C_modeltool(Subject,Condition,'MVC',999);
if ~(exist(Lb2.file_mot, 'file'))
    %=== Export trc
    if  ~exist(Lb2.file_GRF, 'file')
        if 0
            Lb2.f_writeTRCGRF_OS();
        else
            Lb2.f_writeTRC_Mokka(1);
            input('==> Press Enter after trc has been exported...');
            Lb2.f_rotate_shift_trc('x',90,[20 30 44]);
            Lb2.f_writeGRF_OS;
        end
        1
    else
        disp(' ')
        disp('ROT TRC already exported, skip moving TRC export ... ')
    end
    %=== Setup IK
    Lb2.f_setup_IK(1);
    input('==> Press Enter after IK ... ')
else
    disp(' ')
    disp(['Mot already scaled, skip IK  ... ',Lb2.file_mot])
end
%%   Setup up extermal loading
if 0
    Lb2.f_GUI_mot;
    Lb2.f_GUI_c3d;
end


if ~exist(Lb2.file_setup_ExtLoads,'file')
    % Lb2.f_GUI_c3d
    setup_forceplate_LR={...
        {'r','1'},...
            {'l','2'},...
            {'r','3'},...
        };
    Lb2.f_setup_ExtLoads(setup_forceplate_LR);
end
%%  Setup ID and run
Lb2.f_load_model();
Lb2.f_load_res_mot();
Lb2.f_setup_run_ID(6);
Lb2.f_load_res_ID();

%%  RRA

Lb2.f_setup_RRA([0,100]);
Lb2.f_run_RRA();
Lb2.f_load_res_RRA;



%%  View Joint Torques
% Lb2.f_set_T_interest(Lb2.OS.name_coor);
% Lb2.f_fig_subplot_T_interest();
% Lb2.f_fig_plot_T_interest();
% Lb2.f_fig_waterfall_T_interest;

%%
Free_joints={...
    'hip_flexion_r';...
    'hip_adduction_r';...
    'hip_rotation_r';...
    'knee_angle_r';...
    'ankle_angle_r';...
    'subtalar_angle_r';...
    'mtp_angle_r';...
    %
%     'hip_flexion_l';...
%     'hip_adduction_l';...
%     'hip_rotation_l';...
%     'knee_angle_l';...
%     'ankle_angle_l';...
%     'subtalar_angle_l';...
%     'mtp_angle_l'...
    };
Lb2.joints_interest=Free_joints;
Lb2.f_load_res_ID_interest();
Lb2.f_fig_subplot_T_interest_ID();



%%

Lb2.joints_interest=Free_joints;
Lb2.f_load_res_RRA_interest();
Lb2.f_fig_subplot_T_interest_RRA()



%%
if 0
    Lb2.step_length=1;
    indx_act_muscle=[1:92];
    Lb2.f_get_MA_Fvel(indx_act_muscle);
else
    Lb2.f_load_MA_Fvel();
end
%%

Lb2.f_setup_run_SO(1);
% Lb2.f_l
%%
Lb2.f_fig_subplot_res_Fm(Lb2.res_SO);

Lb2.f_fig_subplot_res_a(Lb2.res_SO);


%%
Lb2.f_extract_Analog();
%%
Lb2=C_modeltool(Subject,Condition,'MVC',999);
set_channelname={
    'RecFem',...
    'VasLat',...
    'VasMed',...
    'TibAnt',...
    'LatGas',...
    'MedGas',...
    'LatHam',...
    'MedHam',...
%                     'Wireless Sync'...
%                     'Direct Sync'...
%                     'Pedar_Sync'...
    };


a3=Lb2.f_extract_EMG(set_channelname);
%%
Set_MVC={'MVC','MVC_02'};

Lb2.f_load_MVC(Set_MVC,set_channelname);

a=Lb2.res_MVC;
% Lb2.f_fig_subplot_EMG
%



