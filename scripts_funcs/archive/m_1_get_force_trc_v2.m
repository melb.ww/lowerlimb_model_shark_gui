clear all

%% Load OpenSim libs
import org.opensim.modeling.*

%% Get the path to a C3D file
folder_path='C:\Users\wuw4\OneDrive - The University of Melbourne\Documents\Unimelb_research\Timothy_sayer_data\Tim cleaned\';
subject_='Dev24';
condition_='Barefoot';
filename_input='Dev24 Running 08';
filename_input='Dev24 Cal 01';

%%
folder_path_prod_output=fullfile(folder_path,subject_,condition_,'my_processed');

if exist(folder_path_prod_output, 'dir')~=7
    mkdir(folder_path_prod_output)
end

%%
filename_=strrep(filename_input, ' ', '_');
file_c3d=fullfile([fullfile(folder_path,subject_,condition_,filename_input),'.c3d']);
file_trc=fullfile(folder_path_prod_output,[filename_,'.trc']);
file_grf=fullfile(folder_path_prod_output,[filename_,'_grf.mot']);
file_emg=fullfile(folder_path_prod_output,[filename_,'_EMG.mat']);

%%  trc grf
if 1
    c3d = osimC3D(file_c3d,1);
    c3d.writeTRC(file_trc);
    c3d.writeMOT(file_grf);
end

%%  EMG
if 0
    % ------------Read-------------
    itf = c3dserver();
    openc3d(itf, 0, file_c3d);
    data_analog_set = f_get_c3d_AnalogSet(itf);
    p_load_variables_EMG
    ratio=itf.GetAnalogVideoRatio;
    n_frame=itf.GetVideoFrameRate;
    % ---------Preparation------------
    %%
    EMG=[];
    figure
    for n=1:length(set_channelname)-2
        channelname=char(set_channelname(n));
        
        ind_temp=find(ismember(data_analog_set.set_channel_name,channelname));
        
        
        subplot(8,1,n)
        plot(data_analog_set.time_EMG_adj,abs(data_analog_set.set_ACHANNEL_out(:,ind_temp)))
        title(data_analog_set.set_channel_name(ind_temp))
    end
    
    save(file_emg,'data_analog_set')
end
%%
% ---------Processing----------
if 0
    
    EMG_p=double(EMG);
    figure;plot (EMG_p)
    
    % removeDC
    EMG_p = detrend(EMG_p);
    
    %  'HPF'
    order = 2;
    freq = 25;
    [b_l,a_l] = butter(order, freq/(ratio*n_frame/2), 'high');
    EMG_p = filtfilt(b_l, a_l, EMG_p);
    
    % rectify
    EMG_p=abs(EMG_p);
    
    %  'LPF'
    order = 2;
    freq = 2;
    [b_l,a_l] = butter(order, freq/(ratio*n_frame/2), 'low');
    EMG_p = filtfilt(b_l, a_l, EMG_p);
    
    figure;plot (EMG_p)
end
