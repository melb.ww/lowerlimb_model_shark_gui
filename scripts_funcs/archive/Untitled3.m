extLoadsObject.print([pwd, '\support_files\temp_my_ext_force.xml']);
% ============ ID ============

IDsetupFile=[pwd,'\support_files\setup_id_api_no_model_no_external.xml'];
IDtool=InverseDynamicsTool(IDsetupFile);
IDtool.setResultsDir([pwd,'\support_files\ID_temp']);
IDtool.setModel(my_model)
IDtool=f_Set_ID_mot(IDtool,File_motion );

 IDtool.setExternalLoadsFileName([pwd, '\support_files\temp_my_ext_force.xml']);

IDtool.run();


if 1
    figure
    for n=1:nCoor_SO-1
        subplot(3,2,n)
        
        plot (time_count,T_interest_TS(:,n),'LineWidth',2.5);
        title([num2str(n),'  ',char(Free_joints(n))],'fontsize',10,'Interpreter', 'none');
        xlim([time_count(1),time_count(end)])
        %     ylim([min(T_interest_TS(:))*1.2,max(T_interest_TS(:))*1.2])
        ylim([-200 100])
        %     ylim([-120,35])
        grid on;
    end
    
end