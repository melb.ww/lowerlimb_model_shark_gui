if 1
    ccc
else
    clear all
end


Subject.name='SHARK099';



Subject.mass=68.8776;
Subject.height=0;

Subject.LR=0;
% Subject.LR='L'; %L, R, LR



Condition='Barefeet 1';
Condition='Shoes 1';
% Condition='Barefeet 2';
% Condition='Old Study Shoes 2';%''


if 1
C_modeltool.f_S_repo_C3D(Subject,Condition);
folders_res=C_modeltool.f_S_repo_res(Subject,Condition);
end
%%  Static trc and Scale'
static_trial=C_modeltool(Subject,Condition,'Cal 02');
% static_trial.f_GUI_c3d()
%%
%=== Export trc
static_trial.f_writeTRCGRF_OS;



%=== Scale model

static_trial.f_set_flag_PluginGait(1);
static_trial.f_setup_scale;


static_trial.f_setup_osim_LM(1);
static_trial.f_setup_osim_LM(-1);


%%  Load Move Trial Class


% Lb2=C_modeltool(Subject,Condition,'Trial30');Lb2.setup_forceplate_LR={{'r','2'}};
% Lb2=C_modeltool(Subject,Condition,'Trial31');Lb2.setup_forceplate_LR={{'r','1'}};
% Lb2=C_modeltool(Subject,Condition,'Trial33');Lb2.setup_forceplate_LR={{'r','1'}};
% Lb2=C_modeltool(Subject,Condition,'Trial07');Lb2.setup_forceplate_LR={{'r','1'}};

% Lb2=C_modeltool(Subject,Condition,'Trial10');Lb2.setup_forceplate_LR={{'r','1'}};
% Lb2=C_modeltool(Subject,Condition,'Trial12');Lb2.setup_forceplate_LR={{'r','1'}};
% Lb2=C_modeltool(Subject,Condition,'Trial13');Lb2.setup_forceplate_LR={{'r','1'}};

% Lb2=C_modeltool(Subject,Condition,'Trial14');Lb2.setup_forceplate_LR={{'r','1'}};
% Lb2=C_modeltool(Subject,Condition,'Trial14');Lb2.setup_forceplate_LR={{'r','1'},{'r','3'}};
% Lb2=C_modeltool(Subject,Condition,'Trial14');Lb2.setup_forceplate_LR={{'r','1'}};
% Lb2=C_modeltool(Subject,Condition,'Trial15');Lb2.setup_forceplate_LR={{'r','1'}};

Lb2=C_modeltool(Subject,Condition,'Trial27');Lb2.setup_forceplate_LR={{'r','1'}};
%%
if 0
    JI_temp={...
        'hip_flexion_r';...
        'hip_adduction_r';...
        %     'hip_rotation_r';...
        
        
        'knee_angle_r';...
        'ankle_angle_r';...
        %     'subtalar_angle_r';...
        };
    
    Lb2.joints_interest=JI_temp;
end
%%
if 1 
    Lb2.f_GUI_c3d
end

%%  Motion trc and IK

%=== Export trc
Lb2.f_writeTRCGRF_OS();
% Lb2.ff_Trim_Marker_down();


%=== Setup IK
Lb2.f_setup_IK(1);


if 0
    Lb2.f_GUI_mot;
    Lb2.f_GUI_c3d;
end

%%  Setup up extermal loading
if ~exist(Lb2.file_setup_ExtLoads,'file')
    % Lb2.f_GUI_c3d
    Lb2.f_setup_ExtLoads();
    
end

%%  Setup ID, run and load
if 1
    Lb2.f_load_model();
    Lb2.f_load_res_mot();
    Lb2.f_setup_run_ID(6);
    Lb2.f_load_res_ID();
end
Lb2.f_load_res_ID_interest();

%  Plot moment of Interest
Lb2.f_fig_subplot_T_interest_ID(1); % flag_GRF_event
% Lb2.f_fig_subplot_T_interest_ID_norm(1); % flag_GRF_event

%%  MA for SO

if 1
    %     Lb2.indx_act_muscle=[1:92]; % ALL
    Lb2.f_get_MA_Fvel();
else
    Lb2.f_load_MA_Fvel();
end

%%  SO
if 1
    
    if 1
        Lb2.f_load_res_ID_interest();
        Lb2.f_load_MA_Fvel();
    end
    Lb2.f_setup_run_SO(1);
    Lb2.f_save_res_SO();
else
    Lb2.f_load_res_SO();
end

%%  Plot SO


% Lb2.f_fig_subplot_res_a(Lb2.res_SO,1);% flag_GRF_event=1;
 Lb2.f_fig_subplot_res_Fm(Lb2.res_SO,1);% flag_GRF_event=1;
% Lb2.f_fig_subplot_res_a(Lb2.res_SO,1);

%%  Knee force prepare

% get LR ID
if 1
    Lb2.f_setup_run_ID_LM(6,1);
    Lb2.f_setup_run_ID_LM(6,-1);
end
% get LR MA
if 1
    Lb2.step_length=1;
    Lb2.f_get_MA_LM();
end
%%  Knee force and plot
if 1
    if 1
        Lb2.f_load_res_ID_LM();
        Lb2.f_load_res_SO();
        Lb2.f_load_MA_LM();
    end
    
    Lb2.f_setup_run_KneeLoad();
    Lb2.f_save_res_Knee();
else
    Lb2.f_load_res_Knee();
end
%
flag_GRF_event=0;
Lb2.f_fig_subplot_Knee(flag_GRF_event);

%%  =================================================

%%  get analog
Lb2.f_extract_Analog();

%%  Copy c3d from Nexus to Repo
if 1
    folder_nexus='Z:\My Documents\Vicon Nexus Databases\SHARK Trial\SHARK Trial_PROCESSED\SHARK\';
    C_modeltool.f_S_copyNexusC3D_2_Repo(folder_nexus,' ',Subject,Condition);       
end
%%
C_modeltool.f_S_repo_C3D(Subject,Condition);
if 0
    C_modeltool.f_S_data_summary;
end