ccc
% clear all
% close all
% ccc
Subject.name='SHARK003';
% Subject.mass=0;  % set 0 to load reading from 'subject_data.csv'
Subject.mass=64.3;

% Subject.LR='L'; %L, R
Subject.LR=0; %set 0 to load reading from 'subject_data.csv'

Condition='Barefeet 1';%''

%%  Static trc and Scale'
static_trial=C_modeltool(Subject,Condition,'Cal 01');
%%
%=== Export trc
static_trial.f_writeTRCGRF_OS;

%=== Scale model
static_trial.f_setup_scale;
static_trial.f_setup_osim_LM(1);
static_trial.f_setup_osim_LM(-1);


%%  Load Move Trial Class
% Lb2=C_modeltool(Subject,Condition,'Trial01');Lb2.setup_forceplate_LR={{'r','2'}};
% Lb2=C_modeltool(Subject,Condition,'Trial03');Lb2.setup_forceplate_LR={{'l','1'}};
% % % Lb2=C_modeltool(Subject,Condition,'Trial08');Lb2.setup_forceplate_LR={{'l','2'}};
% Lb2=C_modeltool(Subject,Condition,'Trial19');Lb2.setup_forceplate_LR={{'r','2'},{'l','1'}};
Lb2=C_modeltool(Subject,Condition,'Trial01');Lb2.setup_forceplate_LR={{'l','1'},{'l','3'}};
% Lb2=C_modeltool(Subject,Condition,'Trial11');Lb2.setup_forceplate_LR={{'l','1'},{'r','2'},{'l','3'}};

%%  Motion trc and IK

%=== Export trc
Lb2.f_writeTRCGRF_OS();

%=== Setup IK
Lb2.f_setup_IK(1);


if 0
    Lb2.f_GUI_mot;
    Lb2.f_GUI_c3d;
end

%%  Setup up extermal loading
if ~exist(Lb2.file_setup_ExtLoads,'file')
    % Lb2.f_GUI_c3d
    Lb2.f_setup_ExtLoads();
end

%%  Setup ID, run and load
if 0
    Lb2.f_load_model();
    Lb2.f_load_res_mot();
    Lb2.f_setup_run_ID(6);
    Lb2.f_load_res_ID();
end
Lb2.f_load_res_ID_interest();

%%  RRA
if 0
    Lb2.f_setup_RRA([0,100]);
    Lb2.f_run_RRA();
    Lb2.f_load_res_RRA;
end
if 0
    %     Lb2.joints_interest=Free_joints;
    Lb2.f_load_res_RRA_interest();
    Lb2.f_fig_subplot_T_interest_RRA()
end

%%  Plot moment of Interest
Lb2.f_fig_subplot_T_interest_ID(1); % flag_GRF_event
% Lb2.f_fig_subplot_T_interest_ID_norm(1); % flag_GRF_event

%%  MA for SO

if 1
    %     Lb2.indx_act_muscle=[1:92]; % ALL
    Lb2.f_get_MA_Fvel();
else
    Lb2.f_load_MA_Fvel();
end

%%  SO
if 0
    Lb2.f_setup_run_SO(1);
    Lb2.f_save_res_SO();
else
    Lb2.f_load_res_SO();
end

%%  Plot SO


% Lb2.f_fig_subplot_res_a(Lb2.res_SO,1);% flag_GRF_event=1;
% Lb2.f_fig_subplot_res_Fm(Lb2.res_SO,1);% flag_GRF_event=1;
Lb2.f_fig_subplot_res_a(Lb2.res_SO,1);

%%  Knee force prepare

% get LR ID
if 1
    Lb2.f_setup_run_ID_LM(6,1);
    Lb2.f_setup_run_ID_LM(6,-1);
end
% get LR MA
if 1
    Lb2.step_length=1;
    Lb2.f_get_MA_LM();
end
%%  Knee force and plot

if 1
    Lb2.f_load_res_ID_LM();
    Lb2.f_load_res_SO();
    Lb2.f_load_MA_LM();
    
end

Lb2.f_setup_run_KneeLoad();
flag_GRF_event=1;
Lb2.f_fig_subplot_Knee(flag_GRF_event);

%%  =================================================

%%  get analog
Lb2.f_extract_Analog();

%%  Copy c3d from Nexus to Repo
if 1
    folder_nexus='C:\Users\wuw4\OneDrive - The University of Melbourne\Documents\Unimelb_research\2019_FOLK\Folk trial\FOLK\';
    C_modeltool.f_copyNexusC3D_2_Repo(folder_nexus,'11',Subject,Condition);
end
