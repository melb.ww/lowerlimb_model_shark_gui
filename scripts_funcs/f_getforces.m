function [ F_act_alongtendon,F_pas_alongtendon ] = f_getforces( a,Lmt,lm_dot,indx_actvmuscle )
%% globals
global Lom Lst Fom Vmax 


nor_length=(Lmt-Lst(indx_actvmuscle))./Lom(indx_actvmuscle);
l_temp=exp( -0.5.*((nor_length-1)/0.4).^2);
Fv = 0.1433./ ( 0.1074+exp(-1.409.*sinh(3.2.*lm_dot+1.6 )));
F_act_alongtendon=Fom(indx_actvmuscle).*l_temp.*a'.*Fv;
F_pas_alongtendon=Fom(indx_actvmuscle).*exp(10.*(nor_length-1))./exp(5);

% global my_musc_set myState nMusc my_model
% 
% for index_mus=1:nMusc
%     my_musc_set.get(index_mus-1).setActivation(myState, a(index_mus));
% end
% my_model.equilibrateMuscles(myState);
% for index_mus=1:nMusc
%     F_act_alongtendon(index_mus)=my_musc_set.get(index_mus-1).getActiveFiberForceAlongTendon(myState);
%     F_pas_alongtendon(index_mus)=my_musc_set.get(index_mus-1).getPassiveFiberForceAlongTendon(myState);
% end










end

